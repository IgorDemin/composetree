const val kotlinVersion = "1.3.72"
const val coroutinesVersion = "1.3.7"
const val composeVersion = "0.1.0-dev10"
const val composeKotlinCompilerVersion = "1.3.70-dev-withExperimentalGoogleExtensions-20200424"
