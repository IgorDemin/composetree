package com.dmi.compose

import androidx.compose.Composable
import androidx.ui.core.Modifier

/**
 * Widget for rendering very large trees (any size).
 * Nodes will be created and composed only if they are visible on the screen.
 * After scroll new visible nodes will be added, and old invisible nodes will be discarded
 *
 * [refreshIntent] change this parameter for recompose BigTree
 */
@Composable
fun BigTree(
    initialPosition: () -> BigTreePosition,
    refreshIntent: Any = Any(),
    modifier: Modifier = Modifier
) = BigList(
    getInitial = { initialPosition().asListPosition() },
    refreshIntent = refreshIntent,
    modifier = modifier
)

interface BigTreeItem {
    val position: BigTreePosition

    fun parent(): BigTreeItem?
    fun previousSibling(): BigTreeItem?
    fun nextSibling(): BigTreeItem?
    fun firstChild(): BigTreeItem?
    fun lastChild(): BigTreeItem?

    @Composable
    fun content()
}

interface BigTreePosition : Comparable<BigTreePosition> {
    fun item(): BigTreeItem?
}

fun BigTreeItem.lastChildRecursive(): BigTreeItem = lastChild()?.lastChildRecursive() ?: this

fun BigTreeItem.nextSiblingRecursive(): BigTreeItem? = nextSibling() ?: parent()?.nextSiblingRecursive()

fun BigTreeItem.asListItem(): BigListItem = BigTreeListItem(position.asListPosition(), this)

fun BigTreePosition.asListPosition(): BigListPosition = BigTreeListPosition(this)

private class BigTreeListItem(
    override val position: BigListPosition,
    private val node: BigTreeItem
) : BigListItem {

    override fun previous() = node.previousSibling()?.lastChildRecursive()?.asListItem() ?: node.parent()?.asListItem()

    override fun next() = node.firstChild()?.asListItem() ?: node.nextSiblingRecursive()?.asListItem()

    @Composable
    override fun content() = node.content()
}


private class BigTreeListPosition(private val position: BigTreePosition) : BigListPosition {
    override fun item() = position.item()?.asListItem()

    override fun compareTo(other: BigListPosition): Int {
        other as BigTreeListPosition
        return position.compareTo(other.position)
    }
}
