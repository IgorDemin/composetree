package com.dmi.compose

import android.content.Context
import androidx.compose.Composable
import androidx.compose.Composition
import androidx.compose.CompositionReference
import androidx.compose.FrameManager
import androidx.compose.Recomposer
import androidx.compose.Untracked
import androidx.compose.compositionReference
import androidx.compose.currentComposer
import androidx.compose.remember
import androidx.ui.core.Constraints
import androidx.ui.core.ContextAmbient
import androidx.ui.core.LayoutDirection
import androidx.ui.core.LayoutNode
import androidx.ui.core.Measurable
import androidx.ui.core.MeasureScope
import androidx.ui.core.MeasuringIntrinsicsMeasureBlocks
import androidx.ui.core.Modifier
import androidx.ui.core.Ref
import androidx.ui.core.clipToBounds
import androidx.ui.core.subcomposeInto
import androidx.ui.foundation.gestures.DragDirection
import androidx.ui.foundation.gestures.ScrollableState
import androidx.ui.foundation.gestures.scrollable
import androidx.ui.unit.IntPx
import androidx.ui.unit.ipx
import androidx.ui.unit.px
import androidx.ui.unit.round
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.round

/**
 * [refreshIntent] change this parameter for recompose BigList
 */
@Suppress("UNUSED_PARAMETER")
@Composable
fun BigList(
    getInitial: () -> BigListPosition,
    refreshIntent: Any,
    modifier: Modifier = Modifier
) {
    val state = remember { BigListState(getInitial()) }
    state.recomposer = currentComposer.recomposer
    state.context = ContextAmbient.current
    state.compositionRef = compositionReference()
    state.forceRecompose = true

    LayoutNode(
        modifier = modifier
            .scrollable(
                dragDirection = DragDirection.Vertical,
                scrollableState = ScrollableState(state::onScroll)
            )
            .clipToBounds(),
        ref = state.rootNodeRef,
        measureBlocks = state.measureBlocks
    )

    state.recomposeIfAttached()
}

private fun LayoutNode(
    modifier: Modifier,
    ref: Ref<LayoutNode>,
    measureBlocks: LayoutNode.MeasureBlocks
) = LayoutNode().apply {
    this.modifier = modifier
    this.ref = ref
    this.measureBlocks = measureBlocks
}

/**
 * Position in list between items.
 *
 * previous() of first position should return null
 * next() of last position should return null
 *
 * items may change over time (added new, removed old)
 */
interface BigListItem {
    val position: BigListPosition

    fun previous(): BigListItem?
    fun next(): BigListItem?

    @Composable
    fun content()
}

interface BigListPosition : Comparable<BigListPosition> {
    fun item(): BigListItem?
}

@OptIn(ExperimentalStdlibApi::class)
private class BigListState(initial: BigListPosition) {
    lateinit var recomposer: Recomposer
    lateinit var context: Context
    var compositionRef: CompositionReference? = null

    var forceRecompose = false
    val rootNodeRef = Ref<LayoutNode>()
    val rootNode get() = rootNodeRef.value!!
    val measureBlocks = ListMeasureBlocks()

    val entries = Entries()
    var firstPosition: BigListPosition = initial
    var lastPosition: BigListPosition = initial
    var itemsTop = 0f

    var scrollToBeConsumed = 0f

    fun onScroll(distance: Float): Float {
        scrollToBeConsumed = distance
        rootNode.requestRemeasure()
        rootNode.owner!!.measureAndLayout()
        scrollToBeConsumed = 0F
        return distance
    }

    private inner class ListMeasureBlocks : LayoutNode.NoIntrinsicsMeasureBlocks(
        error = "Intrinsic measurements are not supported"
    ) {
        override fun measure(
            measureScope: MeasureScope,
            measurables: List<Measurable>,
            constraints: Constraints,
            layoutDirection: LayoutDirection
        ): MeasureScope.MeasureResult {
            if (forceRecompose) {
                rootNode.ignoreModelReads { recomposeAll() }
                FrameManager.nextFrame()
            }

            val width = constraints.maxWidth.value
            val height = constraints.maxHeight.value
            val childConstraints = Constraints(maxWidth = width.ipx, maxHeight = IntPx.Infinity)

            // remove all entries for deleted items and add entries for new items
            var actualItem = firstPosition.item()
            val it = entries.listIterator()
            entries.indexShiftWhenAdding = -1

            var item = if (it.hasNext()) it.next().item else null
            loop@ while (item != null) {
                when {
                    actualItem == null || item.position < actualItem.position -> {
                        it.remove()
                        item = if (it.hasNext()) it.next().item else null
                    }
                    item.position > actualItem.position -> {
                        it.add(Entry(actualItem, newNode()))
                        actualItem = actualItem.next()
                    }
                    else -> {
                        item = if (it.hasNext()) it.next().item else null
                        actualItem = actualItem.next()
                    }
                }
            }

            entries.indexShiftWhenAdding = 0

            itemsTop += scrollToBeConsumed
            var itemsBottom = itemsTop

            for (node in rootNode.layoutChildren) {
                node.measure(childConstraints, layoutDirection)
                itemsBottom += node.height.value
            }

            fun addFirst(): Boolean {
                if (itemsTop <= 0) return false
                val firstItem = entries.firstOrNull()?.item
                val previousItem = if (firstItem != null) firstItem.previous() else firstPosition.item()
                previousItem ?: return false

                val entry = Entry(previousItem, newNode())
                entries.add(0, entry)
                entry.measure(childConstraints, layoutDirection)
                itemsTop -= entry.node.height.value
                firstPosition = entry.item.position
                return true
            }

            fun addLast(): Boolean {
                if (itemsBottom >= height) return false
                val lastItem = entries.lastOrNull()?.item
                val nextItem = if (lastItem != null) lastItem.next() else lastPosition.item()
                nextItem ?: return false

                val entry = Entry(nextItem, newNode())
                entries.add(entry)
                entry.measure(childConstraints, layoutDirection)
                itemsBottom += entry.node.height.value
                lastPosition = entry.item.position
                return true
            }

            fun removeFirst(): Boolean {
                val firstNode = entries.firstOrNull()?.node ?: return false
                if (itemsTop + firstNode.height.value > 0) return false

                itemsTop += firstNode.height.value
                entries.removeFirst()
                firstPosition = entries.firstOrNull()?.item?.position ?: lastPosition
                return true
            }

            fun removeLast(): Boolean {
                val lastNode = entries.lastOrNull()?.node ?: return false
                if (itemsBottom - lastNode.height.value < height) return false

                itemsBottom -= lastNode.height.value
                entries.removeLast()
                lastPosition = entries.lastOrNull()?.item?.position ?: firstPosition
                return true
            }

            while (removeFirst()) {
                // just remove next
            }

            while (addFirst()) {
                // just add next
            }

            if (itemsTop > 0) {
                itemsBottom -= itemsTop
                itemsTop = 0F
            }

            while (removeLast()) {
                // just remove next
            }

            while (addLast()) {
                // just add next
            }

            return measureScope.layout(width = width.ipx, height = height.ipx) {
                var y = round(itemsTop)
                rootNode.layoutChildren.forEach {
                    it.place(x = IntPx.Zero, y = y.px.round())
                    y += it.height.value
                }
            }
        }
    }

    fun recomposeIfAttached() {
        if (rootNode.owner != null) {
            recomposeAll()
        }
    }

    private fun recomposeAll() {
        entries.forEach(Entry::compose)
        forceRecompose = false
    }

    private fun newNode() = LayoutNode().apply {
        measureBlocks = MeasuringIntrinsicsMeasureBlocks { measurables, constraints, _ ->
            require(measurables.size <= 1) { "Content must contains single or zero elements" }
            val measurable = measurables.firstOrNull()
            val placeable = measurable?.measure(Constraints(minWidth = constraints.minWidth, maxWidth = constraints.maxWidth))
            val width = (placeable?.width ?: 0.ipx).coerceIn(constraints.minWidth, constraints.maxWidth)
            val height = (placeable?.height ?: 0.ipx).coerceIn(constraints.minHeight, constraints.maxHeight)
            layout(width, height) {
                placeable?.placeAbsolute(0.ipx, 0.ipx)
            }
        }
    }

    private inner class Entries : AbstractList<Entry>() {
        private val list = ArrayList<Entry>()

        override val size: Int get() = list.size
        var indexShiftWhenAdding = 0

        override fun add(index: Int, element: Entry) {
            list.add(index + indexShiftWhenAdding, element)
            rootNode.insertAt(index + indexShiftWhenAdding, element.node)
            element.compose()
        }

        override fun get(index: Int): Entry {
            return list[index]
        }

        override fun removeAt(index: Int): Entry {
            val entry = list.removeAt(index)
            rootNode.removeAt(index, count = 1)
            entry.dispose()
            return entry
        }

        override fun set(index: Int, element: Entry): Entry {
            throw UnsupportedOperationException()
        }
    }

    private inner class Entry(val item: BigListItem, val node: LayoutNode) {
        var composition: Composition? = null
        var constraints: Constraints? = null
        var layoutDirection: LayoutDirection? = null

        fun measure(constraints: Constraints, layoutDirection: LayoutDirection) {
            if (constraints != this.constraints || layoutDirection != this.layoutDirection) {
                node.measure(constraints, layoutDirection)
                this.constraints = constraints
                this.layoutDirection = layoutDirection
            }
        }

        fun compose() {
            composition = subcomposeInto(context, node, recomposer, compositionRef) @Untracked {
                constraints = null
                layoutDirection = null
                item.content()
            }
        }

        fun dispose() {
            composition?.dispose()
        }
    }
}