@file:Suppress("UnstableApiUsage")

plugins {
    id("com.android.application")
    kotlin("android")
}

val config = loadProperties(".sign/sign.properties")
val signEnabled = (config["sign.enabled"] as String?)?.toBoolean() ?: false

android {
    buildFeatures {
        compose = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf("-Xopt-in=kotlin.RequiresOptIn")
    }

    composeOptions {
        kotlinCompilerVersion = composeKotlinCompilerVersion
        kotlinCompilerExtensionVersion = composeVersion
    }

    if (signEnabled) {
        signingConfigs {
            create("main") {
                storeFile = File(rootProject.projectDir, ".sign/" + config["sign.store.file"] as String)
                storePassword = config["sign.store.password"] as String
                keyAlias = config["sign.key.alias"] as String
                keyPassword = config["sign.key.password"] as String
            }
        }
    }

    buildTypes {
        named("release") {
            signingConfig = if (signEnabled) signingConfigs["main"] else null
        }
        named("debug") {
            signingConfig = if (signEnabled) signingConfigs["main"] else null
        }
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.1.0")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
    implementation("androidx.compose:compose-runtime:$composeVersion")
    implementation("androidx.ui:ui-core:$composeVersion")
    implementation("androidx.ui:ui-platform:$composeVersion")
    implementation("androidx.ui:ui-framework:$composeVersion")
    implementation("androidx.ui:ui-layout:$composeVersion")
    implementation("androidx.ui:ui-material:$composeVersion")
    implementation("androidx.ui:ui-foundation:$composeVersion")
    implementation("androidx.ui:ui-animation:$composeVersion")
    implementation("androidx.ui:ui-tooling:$composeVersion")

    implementation(project(":composetree"))
}
