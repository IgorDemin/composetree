package com.dmi.compose.demo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Providers
import androidx.ui.core.setContent
import com.dmi.compose.demo.common.Ambients
import com.dmi.compose.demo.common.Place
import com.dmi.compose.demo.common.PlaceNavigation
import com.dmi.compose.demo.ui.Main
import com.dmi.compose.demo.util.Navigation

class MainActivity : AppCompatActivity() {
    private val navigation: PlaceNavigation = Navigation(Place.BigTree, ::finish)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Providers(
                Ambients.Navigation provides navigation
            ) {
                Main()
            }
        }
    }

    override fun onBackPressed() = navigation.exit()
}
