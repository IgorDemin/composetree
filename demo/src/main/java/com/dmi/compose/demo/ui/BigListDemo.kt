package com.dmi.compose.demo.ui

import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue
import androidx.ui.core.Modifier
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.fillMaxHeight
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.Button
import androidx.ui.material.Surface
import androidx.ui.unit.dp
import com.dmi.compose.BigList
import com.dmi.compose.BigListItem
import com.dmi.compose.BigListPosition

@Composable
fun BigListDemo() {
    val initialIndex = 0
    val range by mutableStateOf(0..100)

    var indexToRemove = 2
    val removed = HashSet<Int>()
    var refreshIntent by mutableStateOf(Any())

    fun previousIndex(index: Int): Int {
        var i = index - 1
        while (i in removed) {
            i--
        }
        return i
    }

    fun nextIndex(index: Int): Int {
        var i = index + 1
        while (i in removed) {
            i++
        }
        return i
    }

    class TestPosition(val index: Int) : BigListPosition {
        override fun item() = TestItem(nextIndex(index - 1))

        override fun compareTo(other: BigListPosition): Int {
            other as TestPosition
            return index.compareTo(other.index)
        }

        inner class TestItem(private var itemIndex: Int) : BigListItem {
            override val position = TestPosition(itemIndex)

            override fun previous(): BigListItem? {
                val indexBefore = previousIndex(itemIndex)
                return if (indexBefore >= range.first) TestItem(indexBefore) else null
            }

            override fun next(): BigListItem? {
                val indexAfter = nextIndex(itemIndex)
                return if (indexAfter <= range.last) TestItem(indexAfter) else null
            }

            @Composable
            override fun content() {
                Text(itemIndex.toString(), modifier = Modifier.fillMaxWidth().padding(16.dp))
            }
        }
    }

    Surface {
        Column {
            Button(
                onClick = {
                    removed.add(indexToRemove++)
                    refreshIntent = Any()
                },
                modifier = Modifier.padding(16.dp)
            ) {
                Text("Remove")
            }
            BigList({ TestPosition(initialIndex) }, refreshIntent, modifier = Modifier.fillMaxWidth().fillMaxHeight())
        }
    }
}
