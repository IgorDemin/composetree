package com.dmi.compose.demo.common

import com.dmi.compose.demo.util.Navigation

sealed class Place {
    object BigTree : Place()
}

typealias PlaceNavigation = Navigation<Place>
