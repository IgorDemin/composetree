package com.dmi.compose.demo.ui

import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue
import androidx.ui.core.Modifier
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.fillMaxHeight
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.Button
import androidx.ui.material.Surface
import androidx.ui.unit.dp
import com.dmi.compose.BigTree
import com.dmi.compose.BigTreeItem
import com.dmi.compose.BigTreePosition

@Composable
fun BigTreeDemo() {
    var addition by mutableStateOf(0)

    val maxLevel = 3
    val levelToCount = intArrayOf(
        1_000_000_000,
        4,
        50,
        2
    )

    class TestPosition(
        private val level: Int,
        private val index: Int,
        private val itemParent: TestPosition.TestItem? = null
    ) : BigTreePosition {
        override fun item() = TestItem(level, index, itemParent)

        override fun compareTo(other: BigTreePosition): Int {
            other as TestPosition
            return when {
                level < other.level -> -1
                level > other.level -> 1
                else -> index.compareTo(other.index)
            }
        }

        inner class TestItem(
            private var itemLevel: Int,
            private var itemIndex: Int,
            private val parent: TestItem?
        ) : BigTreeItem {
            override val position = TestPosition(itemLevel, itemIndex, parent)

            override fun parent() = parent

            override fun previousSibling() = if (itemIndex > 0) TestItem(itemLevel, itemIndex - 1, parent) else null

            override fun nextSibling() = if (itemIndex < levelToCount[itemLevel] - 1) TestItem(itemLevel, itemIndex + 1, parent) else null

            override fun firstChild() = if (itemLevel < maxLevel) TestItem(itemLevel + 1, 0, this) else null

            override fun lastChild() = if (itemLevel < maxLevel) TestItem(itemLevel + 1, levelToCount[itemLevel + 1] - 1, this) else null

            @Composable
            override fun content() {
                val value = itemIndex + addition
                Text(
                    value.toString(),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp + 32.dp * itemLevel, end = 16.dp, top = 16.dp, bottom = 16.dp)
                )
            }
        }
    }

    Surface {
        Column(modifier = Modifier.padding(16.dp)) {
            Button(
                onClick = { addition++ },
                modifier = Modifier.padding(16.dp)
            ) {
                Text("Inc")
            }
            BigTree({ TestPosition(0, 0) }, modifier = Modifier.fillMaxWidth().fillMaxHeight())
        }
    }
}
