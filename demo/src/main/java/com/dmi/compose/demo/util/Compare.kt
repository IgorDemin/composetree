package com.dmi.compose.demo.util

import kotlin.math.min

fun <T : Comparable<T>> compareLists(a: List<T>, b: List<T>): Int {
    for (i in 0 until min(a.size, b.size)) {
        val ai = a[i]
        val bi = b[i]
        val comp = ai.compareTo(bi)
        if (comp != 0) {
            return comp
        }
    }

    return when {
        a.size < b.size -> -1
        a.size > b.size -> 1
        else -> 0
    }
}
