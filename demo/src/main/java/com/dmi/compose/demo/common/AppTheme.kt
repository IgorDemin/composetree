package com.dmi.compose.demo.common

import androidx.compose.Composable
import androidx.ui.graphics.Color
import androidx.ui.material.MaterialTheme
import androidx.ui.material.lightColorPalette

@Composable
fun AppTheme(content: @Composable() () -> Unit) = MaterialTheme(
    colors = lightColorPalette(
        primary = Color(0xFF2196F3),
        primaryVariant = Color(0xFF1976D2),
        secondary = Color(0xFFFFC107),
        secondaryVariant = Color(0xFFFFA000)
    ),
    content = content
)
