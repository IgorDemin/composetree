package com.dmi.compose.demo.ui

import androidx.compose.Composable
import com.dmi.compose.demo.common.AppTheme

@Composable
fun Main() = AppTheme {
    BigTreeExpandableDemo()
}
