package com.dmi.compose.demo.ui

import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.mutableStateOf
import androidx.compose.setValue
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.Image
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.Row
import androidx.ui.layout.fillMaxHeight
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.Button
import androidx.ui.material.Checkbox
import androidx.ui.material.Surface
import androidx.ui.material.ripple.ripple
import androidx.ui.res.vectorResource
import androidx.ui.unit.dp
import com.dmi.compose.BigTree
import com.dmi.compose.BigTreeItem
import com.dmi.compose.BigTreePosition
import com.dmi.compose.demo.util.compareLists
import com.dmi.composetree.demo.R

@Composable
fun BigTreeExpandableDemo() {
    var addition by mutableStateOf(0)

    val maxLevel = 3
    val levelToCount = intArrayOf(
        1_000_000_000,
        4,
        50,
        2
    )

    data class ItemId(val indices: List<Int>)

    var refreshIntent = Any()
    var isCheckMode by mutableStateOf(false)
    val checkedIds = HashSet<ItemId>()
    val expandedIds = HashSet<ItemId>()

    class TestPosition(
        private var level: Int,
        private var index: Int,
        private val parent: TestPosition.TestItem? = null
    ) : BigTreePosition {
        val indices: List<Int> = parent?.indices.orEmpty() + listOf(index)

        override fun item() = TestItem(level, index, parent)

        override fun compareTo(other: BigTreePosition): Int {
            other as TestPosition
            return compareLists(indices, other.indices)
        }

        inner class TestItem(
            private val itemLevel: Int,
            private val itemIndex: Int,
            private val itemParent: TestItem?
        ) : BigTreeItem {
            val indices: List<Int> = itemParent?.indices.orEmpty() + listOf(itemIndex)
            val itemId = ItemId(indices)

            private val isExpanded get() = expandedIds.contains(itemId)

            override val position = TestPosition(itemLevel, itemIndex, itemParent)

            override fun parent() = itemParent

            override fun previousSibling() = if (itemIndex > 0) TestItem(itemLevel, itemIndex - 1, itemParent) else null

            override fun nextSibling() =
                if (itemIndex < levelToCount[itemLevel] - 1) TestItem(itemLevel, itemIndex + 1, itemParent) else null

            override fun firstChild() = if (itemLevel < maxLevel && isExpanded) TestItem(itemLevel + 1, 0, this) else null

            override fun lastChild() =
                if (itemLevel < maxLevel && isExpanded) TestItem(itemLevel + 1, levelToCount[itemLevel + 1] - 1, this) else null

            @Composable
            override fun content() {
                val value = itemIndex + addition

                var isChecked by mutableStateOf(checkedIds.contains(itemId))
                var isExpanded by mutableStateOf(isExpanded)

                Clickable(
                    onClick = {
                        isExpanded = !isExpanded
                        if (isExpanded) {
                            expandedIds.add(itemId)
                        } else {
                            expandedIds.remove(itemId)
                        }
                        refreshIntent = Any()
                    },
                    modifier = Modifier.ripple() + Modifier.fillMaxWidth()
                ) {
                    Row(
                        modifier = Modifier.padding(start = 16.dp + 32.dp * itemLevel, end = 16.dp, top = 16.dp, bottom = 16.dp),
                        verticalGravity = Alignment.CenterVertically
                    ) {
                        val isFolder = itemLevel < maxLevel
                        if (isFolder) {
                            Text(if (isExpanded) "▼" else "➤")
                        }
                        if (isCheckMode) {
                            Checkbox(
                                checked = isChecked,
                                onCheckedChange = {
                                    isChecked = !isChecked
                                    if (isChecked) {
                                        checkedIds.add(itemId)
                                    } else {
                                        checkedIds.remove(itemId)
                                    }
                                },
                                modifier = Modifier.padding(start = 16.dp)
                            )
                        }
                        Image(
                            vectorResource(
                                if (isFolder) R.drawable.ic_baseline_folder_24 else R.drawable.ic_baseline_insert_drive_file_24
                            ),
                            modifier = Modifier.padding(start = 16.dp, end = 16.dp)
                        )
                        Text(value.toString())
                    }
                }
            }
        }
    }

    Surface {
        Column {
            Row(modifier = Modifier.padding(8.dp)) {
                Button(
                    onClick = { addition++ },
                    modifier = Modifier.padding(8.dp)
                ) {
                    Text("Inc")
                }
                Button(
                    onClick = { isCheckMode = !isCheckMode },
                    modifier = Modifier.padding(8.dp)
                ) {
                    Text("Check")
                }
            }
            BigTree({ TestPosition(0, 0) }, refreshIntent = refreshIntent, modifier = Modifier.fillMaxWidth().fillMaxHeight())
        }
    }
}
