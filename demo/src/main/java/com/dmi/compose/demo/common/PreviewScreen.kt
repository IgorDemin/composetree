package com.dmi.compose.demo.common

import androidx.compose.Composable
import androidx.compose.Providers
import androidx.ui.graphics.Color
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Surface
import com.dmi.compose.demo.util.Navigation

@Composable
fun PreviewScreen(
    background: Color = MaterialTheme.colors.surface,
    body: @Composable() () -> Unit
) = Providers(
    Ambients.Navigation provides Navigation(Place.BigTree)
) {
    AppTheme {
        Surface(color = background) {
            body()
        }
    }
}
