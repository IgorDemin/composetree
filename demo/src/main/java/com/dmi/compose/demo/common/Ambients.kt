package com.dmi.compose.demo.common

import androidx.compose.ambientOf

object Ambients {
    val Navigation = ambientOf<PlaceNavigation>()
}
